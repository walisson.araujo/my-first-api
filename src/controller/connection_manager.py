import sqlite3
# https://docs.python.org/3/library/sqlite3.html
# https://docs.python.org/3/library/uuid.html

def connection_start():
    connection = sqlite3.connect("./src/schooldb.db")
    return connection
