from controller.connection_manager import connection_start
from controller.cursoDAO import search_curso_by_name
from model.aluno import aluno


def register_aluno(aluno):
    if aluno.get("id") is None or aluno.get("name") is None or aluno.get("birthday") is None:
        print("O aluno não foi cadastrado, pois algum dado não foi fornecido\n")
    else:
        connection = connection_start()
        client = connection.cursor()
        client.execute(f'INSERT INTO aluno VALUES ("{aluno.get("id")}", "{aluno.get("cpf")}", "{aluno.get("name")}", "{aluno.get("birthday")}", "{aluno.get("rg")}", "{aluno.get("orgao_expedidor")}", "{aluno.get("curso_id")}")'),
        connection.commit()
        connection.close()
        print("Cadastramos o aluno no Banco de Dados!\n")


def alterate_aluno_curso(aluno_cpf, curso_nome):
    if aluno_cpf is None or curso_nome is None:
        print('O Aluno nao pode ser cadastrado, pois o dado não foi fornecido\n')
    else:
        connection = connection_start()
        client =  connection.cursor()
        curso_id = search_curso_by_name(curso_nome)
        aluno_id = search_aluno_by_cpf(aluno_cpf)
        client.execute('UPDATE aluno SET curso_id = ? WHERE id = ?', (curso_id, aluno_id,))
        connection.commit()
        connection.close()
        print(f"Alteramos o curso do aluno {search_aluno_by_cpf(aluno_cpf)}")


def search_aluno_by_cpf(aluno_cpf):
    if aluno_cpf is None:
        print("O CPF não foi encontrado\n")
    else:
        connection = connection_start()
        client = connection.cursor()
        search_stmt = client.execute(f'SELECT id FROM aluno WHERE cpf = ?', (aluno_cpf, )).fetchone()
        connection.close()
        return search_stmt[0]


def show_all_alunos():
    connection = connection_start()
    client = connection.cursor()
    search_stmt = client.execute('SELECT aluno.name, aluno.cpf, curso.name FROM aluno INNER JOIN curso ON curso.id = aluno.curso_id')
    rows = search_stmt.fetchall()
    for row in rows:
        print(row)
    connection.close()
    
def get_aluno_by_cpf(aluno_cpf):
    connection = connection_start()
    client = connection.cursor()
    search_stmt = client.execute('SELECT aluno.name, aluno.cpf, curso.name, disciplina.name '+
                                 'FROM aluno '+
                                 'INNER JOIN curso ON curso.id = aluno.curso_id '+
                                 'INNER JOIN disciplina_dos_alunos ON aluno.id = disciplina_dos_alunos.aluno_id '+
                                 'INNER JOIN disciplina ON disciplina_dos_alunos.disciplina_id = disciplina.id '+
                                 'WHERE aluno.cpf = ?', (aluno_cpf,))
    rows = search_stmt.fetchall()
    for row in rows:
        print(row)
    connection.close()
    
