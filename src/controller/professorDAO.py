from controller.connection_manager import connection_start

def register_professor(professor):
    connection = connection_start()
    client = connection.cursor()
    client.execute(f'INSERT INTO professor VALUES ("{professor.get("id")}", "{professor.get("cpf")}", "{professor.get("name")}", "{professor.get("title")}");')
    connection.commit()
    connection.close()

def search_professor_by_cpf(professor_cpf):
    if professor_cpf is None:
        print("O cpf do professor não pode ser vazio")
        return None
    else:
        connection = connection_start()
        client = connection.cursor()
        search_stmt = client.execute(f'SELECT id FROM professor WHERE cpf = ?', (professor_cpf, )).fetchone()
        connection.close()
        return search_stmt[0]
    
    