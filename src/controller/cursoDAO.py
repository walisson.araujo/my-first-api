from sqlite3 import connect
import sqlite3
from controller.connection_manager import connection_start
from controller.professorDAO import search_professor_by_cpf
from model.curso import curso

def register_curso(curso):
    if curso.get('name') is None or curso.get('creation_data') is None or curso.get('predio') is None:
        print("Nao foi possível cadastrar o curso\n")
    else:
        connection = connection_start()
        client = connection.cursor()
        client.execute(f'INSERT INTO curso (id, name, creation_data, predio) VALUES ("{curso.get("id")}", "{curso.get("name")}", "{curso.get("creation_data")}", "{curso.get("predio")}");')
        connection.commit()
        connection.close()
            
    
def search_curso_by_name(curso_name):
    if curso_name is None:
        print("O id do curso não pode ser vazio")
        return None
    else:
        connection = connection_start()
        client = connection.cursor()
        search_stmt = client.execute(f'SELECT id FROM curso WHERE name = ?', (curso_name, )).fetchone()
        connection.close()
        return search_stmt[0]
    
def alter_coordinator_curso(curso_name, professor_cpf):
    connection = connection_start()
    client =  connection.cursor()
    curso_id = search_curso_by_name(curso_name)
    prof_id = search_professor_by_cpf(professor_cpf)
    client.execute('UPDATE curso SET professor_id = ? WHERE id = ?', (prof_id, curso_id,))
    connection.commit()
    connection.close()
    print(f"Alteramos o professor do curso {search_curso_by_name(curso_name)}")
    
def get_all_alunos_from_curso(curso_name):
    connection = connection_start()
    client = connection.cursor()
    search_stmt = client.execute('SELECT aluno.name, curso.name '+
                                 'FROM aluno '+
                                 'INNER JOIN curso ON curso.id = aluno.curso_id '+
                                 'WHERE curso.name = ?;', (curso_name,))
                                 

	

