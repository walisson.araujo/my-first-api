from re import search
from controller.alunoDAO import search_aluno_by_cpf
from controller.connection_manager import connection_start
from model.disciplina import disciplina

def register_disciplina(disciplina):
    connection = connection_start()
    client = connection.cursor()
    client.execute(f'INSERT INTO disciplina VALUES ("{disciplina.get("id")}", "{disciplina.get("code")}", "{disciplina.get("name")}", "{disciplina.get("description")}", "{disciplina.get("professor_id")}");')
    connection.commit()
    connection.close()


def matriculate_aluno(aluno_cpf, disciplina_code):
    connection = connection_start()
    client = connection.cursor()
    aluno_id = search_aluno_by_cpf(aluno_cpf)
    disciplina_id = search_disciplina_by_code(disciplina_code)
    client.execute('INSERT INTO disciplina_dos_alunos VALUES(?, ?)', (aluno_id, disciplina_id,))
    connection.commit()
    connection.close()
    

def dematriculate_aluno(aluno_cpf, disciplina_code):
    connection = connection_start()
    client = connection.cursor()
    aluno_id = search_aluno_by_cpf(aluno_cpf)
    disciplina_id = search_disciplina_by_code(disciplina_code)
    client.execute('DELETE FROM disciplina_dos_alunos WHERE aluno_id = ? and disciplina_id = ?', (aluno_id, disciplina_id))
    connection.commit()
    connection.close()


def search_disciplina_by_code(disciplina_code):
    if disciplina_code is None:
        print("O CODIGO não foi encontrado!!\n")
    else:
        connection = connection_start()
        client = connection.cursor()
        search_stmt = client.execute(f'SELECT id FROM disciplina WHERE code = ?', (disciplina_code, )).fetchone()
        connection.close()
        return search_stmt[0]


def get_alunos_from_disciplina(disciplina_code):
    connection = connection_start()
    client = connection.cursor()
    disciplina_id = search_disciplina_by_code(disciplina_code)
    search_stmt = client.execute('SELECT aluno.name '+
                                 'FROM '+
                                 'aluno '+
                                 'INNER JOIN disciplina_dos_alunos ON aluno.id = disciplina_dos_alunos.aluno_id '+
                                 'WHERE disciplina_dos_alunos.disciplina_id = ?;', (disciplina_id, ))
    rows = search_stmt.fetchall()
    for row in rows:
        print(row[0])


def ranking_disciplinas():
    connection = connection_start()
    search_stmt = connection.execute('SELECT disciplina.code, disciplina.name, COUNT(*) '+
                                     'FROM disciplina '+
                                     'INNER JOIN disciplina_dos_alunos ON disciplina_dos_alunos.disciplina_id = disciplina.id '+
                                     'GROUP BY disciplina.name '+
                                     'ORDER BY COUNT(*) DESC;')
    rows = search_stmt.fetchall()
    for row in rows:
        print(row)
    
