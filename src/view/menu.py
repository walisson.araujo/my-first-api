from view.menu_register import register_menu
from view.menu_search import search_menu

def primary_menu():
    print("Bem vindo ao sistema da PROGRAD\n")
    print("Para prosseguir, selecione uma opção\n")
    
    action = input('1 -> Modulo de cadastro\n2 -> Modulo de Busca\n3 -> SAIR\n')
    if action == '1':
        register_menu()
    elif action == '2':
        search_menu()
    elif action == '3':
        quit()
    else:
        print("Porfavor, selecione uma opção válida")
        primary_menu()
