
from view import menu
from controller.alunoDAO import get_aluno_by_cpf, search_aluno_by_cpf, show_all_alunos
from controller.cursoDAO import get_all_alunos_from_curso, search_curso_by_name
from controller.disciplinaDAO import get_alunos_from_disciplina, ranking_disciplinas, search_disciplina_by_code



def search_menu():
    print("Bem vindo ao menu de busca, o que você deseja buscar?\n")
    action = input("1 -> Exibir todos os alunos\n"+
                   "2 -> Aluno pelo cpf\n"+
                   "3 -> Alunos pelo curso\n"+
                   "4 -> Disciplina pelo codigo\n"+
                   "5 -> Alunos pela Disciplina\n"+
                   "6 -> RANKING DISCIPLINA PELO NUMERO DE MATRICULADOS\n"+
                   "7 -> VOLTAR\n")
    
    if action == '1':
        show_all_alunos()
        search_menu()
        
    if action == '2':
        aluno_cpf = input("Informe o cpf do aluno:\n")
        print(get_aluno_by_cpf(aluno_cpf))
        search_menu()
        
    if action == '3':
        curso_name = input("Forneça o nome do curso")
        print(get_all_alunos_from_curso(curso_name))
        search_menu()
        
    if action == '4':
        disciplina_code = input("Forneça o codigo da disciplina que deseja buscar\n")
        print(search_disciplina_by_code(disciplina_code))
        search_menu()
        
    if action == '5':
        disciplina_code = input("Forneça o codigo da disciplina que deseja ver os alunos matriculados\n")
        print(get_alunos_from_disciplina(disciplina_code))
        search_menu()
        
    if action == '6':
        ranking_disciplinas()
        search_menu()
    
    else:
        menu.primary_menu()