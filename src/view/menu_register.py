from controller.alunoDAO import alterate_aluno_curso, register_aluno
from controller.disciplinaDAO import dematriculate_aluno, matriculate_aluno, register_disciplina
from controller.professorDAO import register_professor
from model.aluno import aluno
from model.curso import curso
from controller.cursoDAO import alter_coordinator_curso, register_curso
from model.disciplina import disciplina
from model.professor import professor
from view import menu


def register_menu():
    print("Bem vindo ao menu de cadastro, o que você deseja cadastrar?\n")
    action = input("1 -> Curso\n"+
                   "2 -> Aluno\n"+
                   "3 -> Professor\n"+
                   "4 -> Disciplna\n"+
                   "5 -> Alterar Curso de um aluno\n"+
                   "6 -> Cadastrar um Aluno em uma disciplina\n"+
                   "7 -> Desmatricular um Aluno de uma disciplina\n"+
                   "8 -> Alterar Coordenador do Curso\n"+
                   "9 -> VOLTAR\n")
    
    if action == '1':
        print("Forneça os dados do curso:\n")
        new_curso = curso()
        register_curso(new_curso)
        print(f"O curso {new_curso.get('name')} foi cadastrado\n")
        register_menu()
        
    elif action == '2':
        
        print("Forneca os dados do ALuno: \n")
        has_curso = input("O aluno ja tem um curso?\n1-> SIM\n2->NAO\n")
        
        if has_curso == '1':
            curso_id = input("Forneça o nome do curso do aluno:\n")
            new_aluno = aluno(curso_id)
            register_aluno(new_aluno)
            print(f'O aluno {new_aluno.get("name")} foi cadastrado')
            register_menu()
        elif has_curso == '2':
            print("Cadastre um aluno que possui curso")
            register_menu()
            
            
    elif action == '3':
        print("Forneca os dados do Professor:\n")
        new_professor = professor()
        register_professor(new_professor)
        print(f'O professor {new_professor.get("name")} foi cadastrado!\n')
        register_menu()
        
    elif action == '4':
        print("Forneca os dados da Disciplina:\n")
        professor_cpf = input("Informe o cpf do professor responsavel\n")
        new_disciplina = disciplina(professor_cpf)
        register_disciplina(new_disciplina)
        print(f'A disciplina {new_disciplina.get("code")} foi cadastrada\n')
        register_menu()
    
    elif action == '5':
        print("Forneça o cpf e o nome do curso para fazer a alteracao\n")
        aluno5_cpf = input("Forneca o cpf do aluno: ")
        curso_nome = input("\nForneca o nome do curso do aluno: ")
        alterate_aluno_curso(aluno5_cpf, curso_nome)
        print("\n")
        register_menu()
        
    elif action == '6':
        print("Forneça os dados abaixo para cadastrar um aluno em uma disciplina:\n")
        aluno6_cpf = input("Forneca o cpf do aluno: ")
        disciplina_code = input("Forneca o codigo da disciplina: ")
        matriculate_aluno(aluno6_cpf, disciplina_code)
        print("O Aluno foi matriculado com sucesso na disciplina\n")
        register_menu()
        
    elif action == '7':
        print("Forneça os dados abaixo para desmatricular uma unica disciplina de um unico aluno")
        aluno7_cpf = input("Forneça o cpf do aluno: ")
        disciplina7_code = input("Forneça o codigo da disciplina da qual deseja desmatricular o aluno fornecido")
        dematriculate_aluno(aluno7_cpf, disciplina7_code)
        print("O Aluno foi desmatriculado")
        register_menu()
        
    elif action == '8':
        print("Forneça os dados abaixo para alterar o coordenador de um curso")
        curso8_nome = input("Forneça o nome do curso para receber um novo professor: ")
        professor8_cpf = input("Forneça o cpf do professor que será cadastrado como novo coordenador do curso: ")
        alter_coordinator_curso(curso8_nome, professor8_cpf)
        print("O coordenador do curso foi cadastrado!!!\n")
        register_menu()
        
    else:
        menu.primary_menu()