from uuid import  uuid4

from controller.professorDAO import search_professor_by_cpf


def disciplina(professor_cpf):
    
    new_disciplina = {
        'id': None,
        'code': None,
        'name' : None, 
        'description' : None,
        'professor_id' : None
    }
    
    new_disciplina['id'] = uuid4()
    new_disciplina['code'] = input("Forneca o codigo da disciplina\n")
    new_disciplina['name'] = input("Forneca o nome da disciplina\n")
    new_disciplina['description'] = input("Forneça uma breve descrição da disciplina\n") or None
    new_disciplina['professor_id'] = search_professor_by_cpf(professor_cpf)
    
    return new_disciplina
