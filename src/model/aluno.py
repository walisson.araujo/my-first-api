from uuid import uuid4
from controller.cursoDAO import search_curso_by_name

def aluno(curso_name):
    
    new_aluno = {
        'id': None,
        'cpf': None,
        'name' : None, 
        'birthday': None,
        'rg' : None, 
        'orgao_expedidor' : None, 
        'curso_id' : None
    }
    
    new_aluno['id'] = uuid4()
    new_aluno['cpf'] = input("Forneça o CPF do aluno!!!!\n")
    new_aluno['name'] = input("Forneca o nome do aluno\n")
    new_aluno['birthday'] = input("Forneca a data de birthday do aluno: DIA/MES/ANO\n")
    new_aluno['rg'] = input("Forneca o rg do aluno\n") or None
    new_aluno['orgao_expedidor'] = input("Forneça o orgao expedidor do rg\n") or None
    new_aluno['curso_id'] = search_curso_by_name(curso_name)
    return new_aluno
