from uuid import uuid4


def professor():
    
    new_professor = {
        'id': None,
        'cpf': None,
        'name' : None, 
        'title' : None,
    }
    
    new_professor['id'] = uuid4()
    new_professor['cpf'] = input("Forneca o codigo do professor\n")
    new_professor['name'] = input("Forneca o nome do professor\n")
    new_professor['title'] = input("Forneça a titulacao do professor\n")
    
    return new_professor
