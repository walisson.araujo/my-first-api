from uuid import uuid4


def curso():
    
    new_curso = {
        'id': None,
        'name' : None, 
        'creation_data' : None,
        'predio' : None,
        'professor_cpf' : None
    }
    
    new_curso['id'] = str(uuid4())
    new_curso['name'] = input("Forneca o nome do curso:\n")
    new_curso['creation_data'] = input("Forneca a data de criacao do curso: DIA/MES/ANO:\n")
    new_curso['predio'] = input("Forneça o local do curso:\n") or None
    
    return new_curso
