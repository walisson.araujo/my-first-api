
DROP TABLE IF EXISTS curso;
DROP TABLE IF EXISTS professor; 
DROP TABLE IF EXISTS aluno; 
DROP TABLE IF EXISTS disciplina; 
DROP TABLE IF EXISTS disciplina_dos_alunos;

CREATE TABLE  IF NOT EXISTS curso(
	id TEXT PRIMARY KEY, 
	name TEXT UNIQUE NOT NULL,
	creation_data TEXT NOT NULL,
	predio TEXT NOT NULL,
	professor_id, 
	FOREIGN KEY(professor_id)
		REFERENCES professor(id)
	);

CREATE TABLE IF NOT EXISTS professor(
	id TEXT PRIMARY KEY,
	cpf TEXT UNIQUE NOT NULL , 
	name TEXT NOT NULL,
	titulacao TEXT NOT NULL
	);

CREATE TABLE IF NOT EXISTS aluno(
	id TEXT PRIMARY KEY, 
	cpf TEXT UNIQUE NOT NULL,
	name TEXT NOT NULL, 
	aniversario TEXT NOT NULL,
	rg TEXT, 
	orgao_expedidor TEXT, 
	curso_id,
	FOREIGN KEY(curso_id)
		REFERENCES curso (id)
	);

CREATE TABLE IF NOT EXISTS disciplina(
	id TEXT PRIMARY KEY, 
	code TEXT UNIQUE NOT NULL,
	name TEXT NOT NULL, 
	description TEXT,
	professor_id, 
	FOREIGN  KEY(professor_id)
		REFERENCES professor (id)
	);

CREATE TABLE IF NOT EXISTS disciplina_dos_alunos(
	aluno_id, 
	disciplina_id, 
	FOREIGN KEY(disciplina_id)
		REFERENCES disciplina (id),
	FOREIGN KEY(aluno_id)
		REFERENCES aluno (id)
);



SELECT COUNT(*) as cont, disciplina.name, disciplina.code 
FROM disciplina d1, disciplina_dos_alunos d2  
WHERE d1.id = d2.disciplina_id
ORDER BY 
	cont DESC,
	NULLS LAST;
                                     

INSERT INTO curso (id, name, creation_data, predio) VALUES ('12810938931803', 'Engenharia Quimica', '05/02/1986', 'CTEC');

SELECT aluno.name, aluno.cpf, curso.name
FROM
	aluno
INNER JOIN curso ON curso.id = aluno.curso_id;